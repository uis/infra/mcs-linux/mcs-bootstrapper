This is the MCS bootstrapper.  It consists of several parts:

install     
     — The top-level installation script.  
       It invokes, in turn, init.d/*, then install.d/*.

init.d/*
     — first-stage initialization scripts, which fetches and prepares to
       run the second-stage install scripts. 

install.d/*
     — Second-stage installation scripts, which actually perform a local 
       installation.

functions.sh 
     — Utility functions used by all of the above.

syslinux.cfg
     — An example syslinux.cfg file.

mint-bootstrapper.sh 
     — A shell-script that, from a copy of this directory, will create a new
       linux kernel & initramfs pair from first-principles, as well as a 
       secondstage.tar.xz.  (This should be installed and signed on the 
       MCS update server.)

public.key
     — An ASCII-armoured OpenPGP public-key used to validate the second-stage
       tarball and the upstream package repository.

files.pre/*
     — A set of files that will be installed into a new installation prior to
       commencing the bootstrapping procedure proper.  This can be useful to
       pre-seed certain configuration files, for example.

To deploy a new bootstrapper, copy secondstage.tar.xz to
cdn.linux.ds.cam.ac.uk, and, as the "archive" user, put it in
/vol/mcs-repo/www/mcs-linux/$year.  Then run "gpg --sign
secondstage.tar.xz".

If linux or initrd.gz have been changed, they will also need to be
copied to /vol/mcs-repo/www/mcs-linux/bootstrapper, and to anywhere
else that needs them.