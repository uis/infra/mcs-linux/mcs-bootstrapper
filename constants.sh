#!/bin/bash

# The version of MCS Linux being installed.
# Should be a year.
MCS_LINUX_VERSION=2019

# The last two digits of the MCS Linux version being installed.
MCS_LINUX_VERSION_SHORT=${MCS_LINUX_VERSION: -2}

# The hostname that we should install from.
MCS_LINUX_SERVER=linuxupdate${MCS_LINUX_VERSION_SHORT}.ds.cam.ac.uk
