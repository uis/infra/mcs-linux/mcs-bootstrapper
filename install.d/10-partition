#!/bin/bash
#
# 10-partition: Partition local drive(s) for installation.
#
set -e
set -x
set -o pipefail

source functions.sh

# Report available disks to the log.
echo "Block devices available:" >&2
lsblk --output=+hotplug >&2

# Calculate install-drive.
#
# We look for the first disk (not partition) that is:
#
# * Not hotpluggable (which means not removable and not USB/FireWire)
# * At least 100 GiB (107374182400 bytes) in size

lsblk --list --noheadings --bytes --output=name,type,size,hotplug \
      > /tmp/blkdevs

while read name type size hotplug; do
    if [ "${type}" = 'disk' ] &&
       [ "${hotplug}" -eq 0 ] &&
       [ "${size}" -gt 107374182400 ]; then
	ROOTDISK="${name}"
	break
    fi
done < /tmp/blkdevs

if [ -z "${ROOTDISK}" ]; then
  # No block device met our criteria.
  error "Could not find a non-removable disk big enough to install on."
fi

# Write the name of our chosen install device to a new file /dev/rootdisk.
# Note that we only write the block device name to the file, not the
# absolute path.  Other install.d scripts expect this behaviour.
#
echo "${ROOTDISK}" > /dev/rootdisk


# Check that the selected device actually exists, and that we haven't tripped
# over an unexpected condition.
#
if [ ! -e /dev/"${ROOTDISK}" ]; then
  # Uh-oh.
  error "Wanted to install on disk '${ROOTDISK}' which doesn't exist."
fi

#
# Check to see if we already have a sensible-looking partition table.
# (In the common case, we'll be dual-installing with Windows, and the Windows
#  installer will already have set up partitions for us.)
#

ROOTPART1=$(mcs_part "${ROOTDISK}" 1)
ROOTPART3=$(mcs_part "${ROOTDISK}" 3)

if [ -e /dev/"${ROOTPART1}" ] && [ -e /dev/"${ROOTPART3}" ]; then
    # We have the partitions we need.  Onwards!
    true
else

    # Partition install-drive:
    # sdX1: boot (128MB)
    # sdX2: Windows or unused
    # sdx3: Linux (20+GB)
    #
    # sfdisk takes parameters of the form:
    #  <start> <size> <id> <bootable> <c,h,s> <c,h,s>

    # - Create partition 1, start at sector 2048, and be 128MB in size,
    #   type Linux, bootable.

    report "Partitioning $ROOTDISK..."
    sfdisk --label=dos /dev/"${ROOTDISK}" 1>&2 <<EOF
type=82, size=128MiB, bootable
type=da, size=1MiB
type=82
EOF
    # Wait for /dev to catch up.
    #
    report "Waiting for the kernel to catch up..."
    udevadm settle
fi
