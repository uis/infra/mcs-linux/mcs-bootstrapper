#!/bin/bash
#
# 60-postconfigure: Tidy-up after self-maintenance complete
#
set -e
set -x
set -o pipefail

source functions.sh

# Remove the configuration snippet directing APT to log data on STDERR.
rm /target/mnt/root/@/etc/apt/apt.conf.d/99progress

# Re-enable start-stop-daemon and Upstart
#
for i in /sbin/start-stop-daemon /usr/sbin/invoke-rc.d; do
  report "Undiverting $i..."
  chroot /target/mnt/root/@ rm $i;
  chroot /target/mnt/root/@ /usr/bin/dpkg-divert --rename --remove $i 1>&2;
done

# Set root password.
#
# Generated using: printf 'password' | mkpasswd -s -m sha-512 -R 1000000
# It'll take up to a second or two to calculate on current hardware, which 
# I think is reasonable due-diligence..
PASSWORDHASH='$6$rounds=1000000$/KQEGU1NTDmuq$nKtTJr/mU/IgqAa0rr31dFgFBakj2.XA4GzDJ4d97m6ysh7.7Kunl9TFFaCvK/QzqExtWoKwP8DTrwQWU39xn1'
#
# Remove existing root entry from /etc/shadow
#
grep -ve '^root:' /target/mnt/root/@/etc/shadow > /tmp/shadow.part
#
# Write new /etc/shadow.
#
echo "root:${PASSWORDHASH}:15715:0:99999:7:::" > /target/mnt/root/@/etc/shadow
#
# Append non-root components of existing /etc/shadow back.
#
cat /tmp/shadow.part >> /target/mnt/root/@/etc/shadow
