#!/bin/bash
#
# 40-preconfigure: Prepare new root for chroot operations
#
set -e
set -x
set -o pipefail

source functions.sh

# Bind-mount virtual filesystems: proc,sys,dev,run.
#
for i in proc sys sys/kernel/debug sys/kernel/security dev dev/pts run; do
  report "Bind mounting $i..."
  mount -o bind /${i} /target/${i}; 
done

# Symlink /etc/mtab to /proc/mounts in the chroot.
#
report "Symlinking /etc/mtab -> ../proc/self/mounts..."
chroot /target ln -sf ../proc/self/mounts /etc/mtab

# Divert daemon-starting utilities
#
for i in /sbin/start-stop-daemon /usr/sbin/invoke-rc.d; do
  report "Diverting $i in target filesystem..."
  chroot /target /usr/bin/dpkg-divert --rename $i 1>&2
  chroot /target /bin/ln -s /bin/true $i;
done

# Prime the root filesystem with important configuration data
#
report "Copying seed configuration files (again)..."
cp -PR files.pre/. /target

# Symlink /etc/apt/sources.list to /run/sources.list in the chroot.
#
report "Symlinking /etc/apt/sources.list -> /run/sources.list..."
chroot /target ln -sf ../../run/sources.list /etc/apt/sources.list

## Configure APT:
# Install MCS key
#
report "Installing MCS key..."
chroot /target apt-key add - < public.key 1>&2

# Operate non-interactively
#
export DEBIAN_FRONTEND=noninteractive

# Report progress in a machine-readable form to FH 3.
# WARNING: We must ensure that this filehandle is open by the time APT runs,
#          or it will fail with an error.
#
echo "APT::Status-Fd \"3\";" > /target/etc/apt/apt.conf.d/99progress

# Install sources.list.
#
report "Fetching repository configuration..."
securefetch \
  https://${MCS_LINUX_SERVER}/mcs-linux/${MCS_LINUX_VERSION}/sources.list.gpg \
  /target/run/sources.list 1>&2 \
  || error "Unable to securely fetch sources.list."

# Update package lists.
#
exec 3> >( report_apt_progress "Downloading lists of available packages..." )
chroot /target apt-get update >&2
exec 3>&-

# Configure timezone and locale.
#
exec 3> >( report_apt_progress "Configuring timezone and locale..." )
chroot /target apt-get -y install tzdata console-setup >&2
exec 3>&-
