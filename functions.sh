#!/bin/bash
source constants.sh

report() {
  # Extract the basename of the script doing the calling
  SCRIPT=${0##*\/}
  MSG=$1

  # Copy the message to the log.
  echo "${SCRIPT}: ${MSG}" >&2

  # Nicely print the message on the screen.
  whiptail --backtitle "MCS Linux ${MCS_LINUX_VERSION} Installer" \
    --title "$SCRIPT" \
    --infobox "$MSG" \
    8 60
}

report_debootstrap() {
  # Parse the progress output from debootstrap (as emitted on FD 3 when
  # '--debian-installer' is passed on the commandline) and emit messages
  # suitable for consumption by 'whiptail --gauge'.

  SCRIPT=${0##*\/}
  MSG=$1

  (
    PROGRESS=0
    STAGE=0
    STAGETOTAL=11
    PF=""
    IF=""
    IA=""

    # For every line of input:
    while read prefix value; do

      # We've been given some progress data.
      if [ "$prefix" == 'P:' ]; then

        a=$(echo "${value}" | cut -f1 -d\ )
        b=$(echo "${value}" | cut -f2 -d\ )
        tag=$(echo "${value}" | cut -f3 -d\ )

        # Identify which stage of the installation we're in.
        # The stages go:
        #   1.  DOWNREL
        #   2.  DOWNRELSIG
        #   3.  DOWNPKGS
        #   4.  SIZEDEBS
        #   5.  DOWNDEBS
        #   6.  EXTRACTPKGS
        #   7.  INSTCORE
        #   8.  UNPACKREQ
        #   9.  CONFREQ
        #   10. UNPACKBASE
        #   11. CONFBASE

        # For the purposes of calculating progress, we keep track of the number of
        # stages thus far *completed*.

        case "${tag}" in
        'DOWNREL')
          STAGE=0
          ;;
        'DOWNRELSIG')
          STAGE=1
          ;;
        'DOWNPKGS')
          STAGE=2
          ;;
        'SIZEDEBS')
          STAGE=3
          ;;
        'DOWNDEBS')
          STAGE=4
          ;;
        'EXTRACTPKGS')
          STAGE=5
          ;;
        'INSTCORE')
          STAGE=6
          ;;
        'UNPACKREQ')
          STAGE=7
          ;;
        'CONFREQ')
          STAGE=8
          ;;
        'UNPACKBASE')
          STAGE=9
          ;;
        'CONFBASE')
          STAGE=10
          ;;
        esac

        PROGRESS=$(( ($STAGE * 100 / $STAGETOTAL ) + (100 * $a / ($STAGETOTAL * $b)) ))
      fi

      # We're being given some informational data.
      if [ "$prefix" == 'I:' ]; then
        IA=""
      fi

      # We're being given some parameters for an informational message.
      if [ "$prefix" == 'IA:' ]; then
        IA="${IA}${value} "
      fi

      # We're being given a format string for progress data.
      if [ "$prefix" == 'PF:' ]; then
        echo 'XXX'
        echo $PROGRESS
        echo
        echo "${MSG}"
        echo "${value}"
        echo 'XXX'
      fi

      # We're being given a format string for informational data.
      if [ "$prefix" == 'IF:' ]; then
        echo 'XXX'
        echo $PROGRESS
        echo
        echo "${MSG}"
        printf "${value}\n" $IA
        echo 'XXX'
        # Also print a copy of the informational message to STDERR for
        # consumption by syslog.
        printf "debootstrap: ${value}\n" $IA >&2
      fi
    done
  ) | whiptail --backtitle "MCS Linux ${MCS_LINUX_VERSION} Installer" --title "$SCRIPT" --gauge "\n$MSG\n\n" 8 60 0
}

report_apt_progress() {
  # This is designed to capture the output of APT when it's reporting
  # machine-readable status information to STDOUT.  Unfortunately, we couldn't
  # reuse STDERR, as some packages (python2.7-minimal in testing) fail their
  # postinsts in this case.
  SCRIPT=${0##*\/}
  MSG=$1
  (
    while read log; do

      # Parse the message.
      type=$(echo "$log" | cut -f1 -d:)
      thing=$(echo "$log" | cut -f2 -d:)
      percentage=$(echo "$log" | cut -f3 -d:)
      message=$(echo "$log" | cut -f4 -d:)

      # The percentage value is a float.  We can only handle integers.
      # Strip any trailing fraction.
      percentage=${percentage%%.*}

      # Download progress
      if [ "$type" == 'dlstatus' ]; then
        echo 'XXX'
        echo $percentage
        echo
        echo "$MSG"
        echo "$message"
        echo 'XXX'
        echo "apt: $message [${percentage}%]" >&2
      fi

      # Package installation progress
      if [ "$type" == 'pmstatus' ]; then
        echo 'XXX'
        echo $percentage
        echo
        echo "$MSG"
        echo "$message"
        echo 'XXX'
        echo "apt: $message [${percentage}%]" >&2
      fi
    done
  ) | whiptail --backtitle "MCS Linux ${MCS_LINUX_VERSION} Installer" --title "$SCRIPT" --gauge "\n$MSG\n\n" 8 60 0
}

httpevent() {
  EVENT="${1}"
  EVENTURL="http://imaging.ds.cam.ac.uk/auto/report.php?e="

  # Report an HTTP event.
  set +e
  result=$(wget -O - "${EVENTURL}${EVENT}" 1>&2)
  set -e

  echo "Logged state '$EVENT', result '$result', return code '$?'" 1>&2 
}

error() {
  # Extract the basename of the script doing the calling
  SCRIPT=${0##*\/}
  MSG="ERROR: $1"

  httpevent imglinux.action.fail  

  # We've seen cases where shutdown failed, due to stuck processes in disk-wait?
  # There appears to be a race / artifact with the current reboot process, which
  # we don't understand.
  # Therefore, rather than using `shutdown -r` to reboot the host, use a very
  # small bash script.
  bash <<'EOF' &
echo 'Machine will auto-reboot in 5 minutes.' >&2
sleep 300
echo s > /proc/sysrq-trigger
sleep 1
echo u > /proc/sysrq-trigger
sleep 1
echo b > /proc/sysrq-trigger
EOF

  # Copy the error message to STDERR.
  echo "$MSG" >&2

  whiptail --backtitle "MCS Linux ${MCS_LINUX_VERSION} Installer" \
    --title "$SCRIPT: $MSG" \
    --scrolltext \
    --ok-button "Reboot" \
    --textbox /var/log/mcs-bootstrap.log 40 70
  
  echo s > /proc/sysrq-trigger
  sleep 1
  echo u > /proc/sysrq-trigger
  sleep 1
  echo b > /proc/sysrq-trigger
  # This point will never be reached; sysrq-B will cause a hard auto-reboot.
}

# Fetch a clear-signed GPG file, and verify its authenticity.
securefetch() {
  # $1: URL to fetch.
  # $2: Filename to save the unwrapped payload as.
  URL="${1}"
  UNWRAPPEDFILE="${2}"
  
  echo "Securely fetching URL '${URL}' to location '${UNWRAPPEDFILE}'..." >&2

  # Ensure that the trusted public-key is loaded.
  gpg --import < public.key

  # Filename to save the transitory copy as.
  SIGNEDFILE=$(mktemp -t signedfile.XXXXXXXX)

  # Fetch the URL
  wget -q "${URL}" -O "${SIGNEDFILE}"

  # Verify the signature.
  statusfile=$(mktemp -t gpg-verify.XXXXXXXX)
  gpg --status-file "${statusfile}" --batch \
    --trust-model=always -o - < "${SIGNEDFILE}" > "${UNWRAPPEDFILE}"

  # If validation fails, abort!
  if grep -q '^\[GNUPG:] VALIDSIG ' "${statusfile}" ; then
    echo "File fetched and validated successfully, proceeding." >&2
    RETURN_CODE=0
  else
    echo "Failed to verify file!" >&2
    RETURN_CODE=1
  fi

  # Tidy up.
  rm -f "${statusfile}" "${SIGNEDFILE}"

  return $RETURN_CODE
}

# Calculate a device name given a root device and the numbered partition.
# For example:
#	hdc, 3 → hdc3
#	sda, 2 → sda2
#	sdb, 4 → sdb4
#	nvme0n1, 2 → nvme0n1p2
mcs_part() {
  # $1: device name
  # $2: partition number.
  DEV="${1}"
  PART="${2}"

  if   [ "${DEV:0:2}" = "sd" ]; then
    echo "${DEV}${PART}"
  elif [ "${DEV:0:2}" = "hd" ]; then
    echo "${DEV}${PART}"
  elif [ "${DEV:0:4}" = "nvme" ]; then
    echo "${DEV}p${PART}"
  else
    echo "ERROR: unrecognised device naming convention; cannot calculate name of partition '${PART}' of device '${DEV}'!"
 fi
}

