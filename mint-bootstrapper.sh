#!/bin/bash
#
# This script creates a bootstrap image and second-stage tarball from 
# first-principes.

set -e
set -o pipefail

# Caller: you can set the 'DEBUG' and 'TWOSTAGE' environment variables to
# override default behaviour!
DEBUG=${DEBUG:-0}
TWOSTAGE=${TWOSTAGE:-1}
GITVERSION=$(git log -n1 --format=%H)
TIMESTAMP=$(date)
RELEASE="bionic"
MIRROR="http://www-uxsup.csx.cam.ac.uk/pub/linux/ubuntu"
SECONDSTAGE="install.d files.pre"
TARGET=$(mktemp -d -t bootstrapper.XXXXXX)
CHROOT=${TARGET}/chroot
mkdir ${CHROOT}

echo "Checking architecture..."
myarch="$(dpkg --print-architecture)"
if ! [ "${myarch}" = "amd64" ]; then
  echo "ERROR: MCS Linux can only be made on an amd64 system." >&2
  exit 1
fi

echo "Checking that we're root..."
if [ ! $UID -eq 0 ]; then
  echo "ERROR: must be root to prepare a bootstrap image."
  exit 1
fi

echo "Preseeding configuration files into ${CHROOT}..."
cp -PR files.pre/. ${CHROOT}

echo "Bootstrapping the bootstrapper..."
# We _were_ using the simple 'linux-image', but we're using a backported kernel
# for its additional capabilities / hardware support / bugfixes.
#
# Note: we can only pull packages from the mainstream 'bionic' repository;
#       because of a mismatch between the design of debootsrap and the Ubuntu
#       repository layout, we can't reference anything from 'bionic-updates',
#       such as more modern kernels.

eatmydata \
  debootstrap \
    --components=main,restricted,universe,multiverse \
    --include=btrfs-tools,debootstrap,eatmydata,wget,mbr,network-manager,openssh-server,xz-utils,patch,ntpdate,ca-certificates,ifupdown,resolvconf,unbound,gpg,gnupg \
    "${RELEASE}" \
    "${CHROOT}" \
    "${MIRROR}"

echo "Set up bind mounts..."
mount -o bind /proc ${CHROOT}/proc

echo "Setting up mtab symlink..."
chroot "${CHROOT}" ln -sf /proc/mounts /etc/mtab

echo "Setting up sources.list..."
cat <<EOF > ${CHROOT}/etc/apt/sources.list
deb ${MIRROR} bionic main restricted universe multiverse
deb ${MIRROR} bionic-updates main restricted universe multiverse
deb ${MIRROR} bionic-security main restricted universe multiverse
EOF

echo "Installing additional packages..."
chroot "${CHROOT}" apt-get update
chroot "${CHROOT}" apt-get -y -o "APT::Install-Recommends=0" install linux-image-generic-hwe-18.04 rsyslog rsyslog-gnutls

echo "Unbinding mounts..."
umount ${CHROOT}/proc

echo "Prune unneeded files..."
for path in \
  '/var/cache/apt/archives/*.deb' \
  '/var/cache/apt/*pkgcache.bin' \
  '/var/lib/apt/lists/*' \
  '/var/log/*.log' \
  '/usr/share/man' \
  '/usr/share/doc' \
  '/boot/System.map*' \
  '/boot/initrd.img*' ;
do
  rm -Rf "${CHROOT}"/$path; 
done

echo "Prime configuration..."

cat <<'EOF' > ${CHROOT}/etc/shadow
root:$6$rounds=1000000$/KQEGU1NTDmuq$nKtTJr/mU/IgqAa0rr31dFgFBakj2.XA4GzDJ4d97m6ysh7.7Kunl9TFFaCvK/QzqExtWoKwP8DTrwQWU39xn1:15847:0:99999:7:::
daemon:*:15847:0:99999:7:::
bin:*:15847:0:99999:7:::
sys:*:15847:0:99999:7:::
sync:*:15847:0:99999:7:::
games:*:15847:0:99999:7:::
man:*:15847:0:99999:7:::
lp:*:15847:0:99999:7:::
mail:*:15847:0:99999:7:::
news:*:15847:0:99999:7:::
uucp:*:15847:0:99999:7:::
proxy:*:15847:0:99999:7:::
www-data:*:15847:0:99999:7:::
backup:*:15847:0:99999:7:::
list:*:15847:0:99999:7:::
irc:*:15847:0:99999:7:::
gnats:*:15847:0:99999:7:::
nobody:*:15847:0:99999:7:::
libuuid:!:15847:0:99999:7:::
syslog:*:15847:0:99999:7:::
EOF

cat <<'EOF' > ${CHROOT}/etc/NetworkManager/NetworkManager.conf
[main]
plugins=keyfile,ifupdown
rc-manager=resolvconf

[ifupdown]
managed=true
EOF

# Disable unwanted NetworkManager configuration snippets supplied by upstream
## Forces use of systemd-resolved.
chroot "${CHROOT}" rm -f /usr/lib/NetworkManager/conf.d/10-dns-resolved.conf
## Forces local devices, other than wireless devices, to be unmanaged by NetworkManager.
chroot "${CHROOT}" rm -f /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf

# Remove netplan, which we don't use.
chroot "${CHROOT}" apt -y purge netplan.io

# Disable unwanted systemd services
chroot "${CHROOT}" systemctl disable systemd-timesyncd
chroot "${CHROOT}" systemctl disable systemd-resolved

cat <<'EOF' > ${CHROOT}/etc/fstab
# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>     <options>       <dump>  <pass>
tmpfs           /tmp            tmpfs      defaults        0       0
sysfs           /sys            sysfs      defaults        0       0
proc            /proc           proc       defaults        0       0
udev            /dev            devtmpfs   defaults        0       0
none            /dev/pts        devpts     defaults        0       0
EOF

mkdir -p ${CHROOT}/root/.ssh
cat <<'EOF' > ${CHROOT}/root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKkqyBjMahY1fldJFYc7WqGY4JGgPxPlxSphHK65zSC6gRnwHyqHgs0bWIuJF7Jwskprx8Uzb1RwdKNaL80uc0lMscyCVzzHuZuSNn8HzBCenkU13j53CAX/pj8O3COiSiO3v8CSN2yDNT9lxxeljPfM/pwmyHG4WrbSkZBpHxP/zgpuRimq2igXHdH0VV9R1dq1Q3oJTovF6iNt6cAAegrU7DwGDPcERKzVDbANHCQpWEGcU4ryPvW2fQtx233AMePXNbexEwHAQvXfNsparU3+/+N2bveLOwxZDbDbdWjNJqldrwRJ801Q6T74vDkga20rLIVdQDOQT92sxiYFNQwgwKIX5yBDevbObONMcDLyi0DT3f8eiH9pSXqGBbYTqHvdT/+bBSmt6yQ9CWt3pdyEN0h5nZnVoyh2BZL7WNaj0xX4FZL0Znq9uykYQm1HPgoELvPG2C6bXHu0KFEAwzQoE12RIKTAq7wFU9lMatRRZvNPgmKOA4otYi9J3hdv+quCpYPMtQrKWCTJCtnZhmfbuWxmd2bLhUpCqgqstJjECE3+pzBAHsUodhO5DFz5tSvW0koYn7NGj+OBnQnwfLOmpliT62QwkEvIWRCHeiV0dUxWfTJxlzxatnCdHjexSQsabpgvg4XqclUzu5ED34vjw3b3+6RrYxoEymQc4CvQ== MCS Linux (2018)
EOF

# Append restriction to SSHD configuration file to disable
# PasswordAuthentication as a supported method.
cat <<'EOF' >> ${CHROOT}/etc/ssh/sshd_config

# Disable PasswardAuthentication, leaving only public-key authentication.
# This will hopefully cause attackers attempting to brute-force root
# passwords to go elsewhere.
PasswordAuthentication no
EOF

# Configure syslog.
echo "Configuring rsyslog..."
cat <<'EOF' > ${CHROOT}/etc/rsyslog.d/mcs-bootstrap.conf
#
# Log using the FQDN of a host, not its shortname.
#
$PreserveFQDN on
#
# Buffer messages locally.
#
$WorkDirectory /var/spool/rsyslog # Buffer files here
$ActionQueueFileName mcslinux     # Unique name prefix for spool files
$ActionQueueMaxDiskSpace 25m      # 25mb space limit (use as much as possible)
$ActionQueueSaveOnShutdown off    # save messages to disk on shutdown
$ActionQueueType LinkedList       # run asynchronously
$ActionResumeRetryCount -1        # infinite retries if host is down
#
# MCS Linux diverts all of its syslog messages to a separate loghost over TLS.
#
$DefaultNetstreamDriver         gtls
$DefaultNetstreamDriverCAFile   /etc/ssl/certs/ca-certificates.crt
$ActionSendStreamDriverMode     1 # (insist on TLS)
$ActionSendStreamDriverAuthMode x509/name # (verify peer certificate)
#
*.*     @@linuxloghost.ds.cam.ac.uk:6514
*.*     ~
EOF

# Delay start of rsyslog until after networking is up, to try to ensure that the
# hostname reported to the syslog server is correct.
mkdir -p "${CHROOT}/etc/systemd/system/rsyslog.service.d"
cat <<'EOF' > ${CHROOT}/etc/systemd/system/rsyslog.service.d/10-wait-for-network.conf
[Unit]
After=network-online.target nss-lookup.target
Requires=network-online.target nss-lookup.target

[Service]
# Wait for our hostname to not be 'localhost'.
ExecStartPre=/bin/bash -c 'while [ "$(hostname)" = "localhost" ]; do echo "Waiting for hostname to be set..."; sleep 1; done'
EOF

# Set 'static' hostname to 'localhost', to force systemd to fall-back to
# using the DHCP-supplied 'transient' hostname as the effective one.
echo "localhost" > ${CHROOT}/etc/hostname

echo "Installing bootstrapper scripts..."
mkdir "${CHROOT}"/mcs-bootstrapper
cp -R . "${CHROOT}"/mcs-bootstrapper

echo "Write version-number..."
echo "${GITVERSION}" > "${CHROOT}"/mcs-bootstrapper/version

echo "Write build-time..."
echo "${TIMESTAMP}" > "${CHROOT}"/mcs-bootstrapper/builddate


# Only mint a two-stage image if TWOSTAGE is set to non-zero.
if [ ! "${TWOSTAGE}" -eq "0" ]; then
    echo "Minting a two-stage image; see TWOSTAGE variable."
    echo "Minting new second-stage tarball..."

    if [ "$DEBUG" -eq 0 ]; then
        echo "Using full compression as we're not a DEBUG build."
        tar -cvf - --owner=root:0 --group=root:0 ${SECONDSTAGE} | xz -9 -c > ${TARGET}/secondstage.tar.xz
    else
        echo "Not using full compression as we're a DEBUG build."
        tar -cvf - --owner=root:0 --group=root:0 ${SECONDSTAGE} | xz -0 -c > ${TARGET}/secondstage.tar.xz
    fi

    echo "Moving second-stage scripts and files.pre sideways."
    mv "${CHROOT}"/mcs-bootstrapper/install.d "${CHROOT}"/mcs-bootstrapper/install.d-disabled
    mv "${CHROOT}"/mcs-bootstrapper/files.pre "${CHROOT}"/mcs-bootstrapper/files.pre-disabled
else
    echo "Not minting a two-stage image; see TWOSTAGE variable."
    echo "Removing init.d/*-fetch from chroot..."
    rm -Rf "${CHROOT}"/mcs-bootstrapper/init.d/*-fetch
fi

echo "Symlink /init..."
# Note: if you don't do this, then /init will not exist — which causes the
#       kernel to treat the ram blob as an initrd rather than an initramfs,
#       which will fail fairly spectacularly.
pushd "${CHROOT}"
ln -s sbin/init init
popd

# This is important, as otherwise the DNS configuration of the bootstrap
# host will trump what we get at machine bootstrap-time, which used to
# cause nasty problems when the DNS was split.
echo "Fixing-up resolvconf configuration to not include a tail..."
rm -f ${CHROOT}/etc/resolvconf/resolv.conf.d/tail

# This is also important, as otherwise the running DNS server configuration
# will not be set correctly if Unbound starts up before the DHCP completes.
# (It's not clear why the executable permissions aren't correctly set
#  automatically?)
echo "Fixing-up permissions on unbound resolvconf script..."
chmod 755 ${CHROOT}/etc/resolvconf/update.d/unbound

echo "Add mcs-bootstrapper systemd service..."
cat <<'EOF' > ${CHROOT}/lib/systemd/system/mcs-bootstrapper.service
[Unit]
Description=MCS bootstrapper
Requires=multi-user.target
After=multi-user.target

[Service]
WorkingDirectory=/mcs-bootstrapper
StandardInput=tty-force
StandardOutput=tty
StandardError=syslog
TTYPath=/dev/tty1
# Don't try to run "-/bin/plymouth quit" directly, as even with
# a - prefix, the failure of existance of the plymouth binary
# causes the unit as a whole to fail.
ExecStartPre=-/bin/sh -c "plymouth quit"
ExecStartPre=-/bin/chvt 1
ExecStart=/mcs-bootstrapper/install
Type=simple
RemainAfterExit=yes

[Install]
WantedBy=mcs-bootstrap.target
EOF

echo "Add mcs-bootstrapper systemd target..."
cat <<'EOF' > ${CHROOT}/lib/systemd/system/mcs-bootstrap.target
[Unit]
Description=Run MCS bootstrapper
Requires=multi-user.target mcs-bootstrapper.service
After=multi-user.target mcs-bootstrapper.service
EOF

if [ "$DEBUG" -eq 0 ] ; then
    echo "Set default target to mcs-bootstrap.target..."
    chroot ${CHROOT} systemctl set-default mcs-bootstrap.target
    chroot ${CHROOT} systemctl enable mcs-bootstrapper.service
    echo "Disabling console on tty1..."
    chroot ${CHROOT} systemctl mask getty@tty1.service
else
    echo "Not adding boot-time hook to run bootstrapper; see DEBUG variable."
fi

echo "Disable IPv6 privacy addresses..."
rm ${CHROOT}/etc/sysctl.d/10-ipv6-privacy.conf

echo "Reset the status-display colour palette to the Debian defaults..."
chroot ${CHROOT} update-alternatives --set newt-palette /etc/newt/palette.original

echo "Extract kernel..."
mv "${CHROOT}"/boot/vmlinuz* ${TARGET}/linux

echo "Build compressed image..."
pushd "${CHROOT}"
if [ "$DEBUG" -eq 0 ]; then
    echo "Using full compression as we're not a DEBUG build."
    (find . | cpio -o -H newc --quiet | xz -9 --check=crc32) > ${TARGET}/initrd.xz
else
    echo "Not using full compression as we're a DEBUG build."
    (find . | cpio -o -H newc --quiet | xz -0 --check=crc32) > ${TARGET}/initrd.xz
fi

popd

echo "New image constructed:"
ls -lah ${TARGET}
echo "To clean up, delete '${TARGET}' once you've copied everything of interest."
